# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Name:           pyspringboard
Version:        0.0.1
Release:        1%{?dist}
Summary:        Platform for rapidly dispersing project configuration

License:        BSD
URL:            https://bitbucket.org/pysprings/pyspringboard
Source0:        pyspringboard-%{version}.tar.gz

BuildArch:      noarch

Requires:       python-virtualenv
Requires:       git
Requires:       python-sh

BuildRequires:  python-devel

# build requirements for testing
BuildRequires:  pytest
BuildRequires:  python-sh
BuildRequires:  git
BuildRequires:  python-virtualenv

%description
Platform for rapidly dispersing project configuration

%prep
%setup -q


%build
%{__python} setup.py build

%check
%{__python} setup.py test

%install
rm -rf %{buildroot}
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

%clean
rm -rf %{buildroot}

%files
%doc
# For noarch packages: sitelib
%{python_sitelib}/*
%attr(755,root,root) %{_bindir}/pyspring

%changelog
* Sat Dec  7 2013 Tim Flink <tflink@fedoraproject.org> - 0.0.1-1
- initial package for pyspringboard
