Concept of Operations
=====================
 - update index cache
 - git clone repository
 - verify repository
     - Test bad GPG signature
     - How to get key into system?
 - read configuration
 - execute configuration
     - get deps
     - create virtualenv
         - Add to .gitignore?
     - run any additional setup
