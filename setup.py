from setuptools import setup, Command
import codecs
import re
import os

here = os.path.abspath(os.path.dirname(__file__))

class PyTest(Command):
    # this method is courtesy of AutoQA and is GPL2+
    user_options = []
    def initialize_options(self):
        pass
    def finalize_options(self):
        pass
    def run(self):
        import subprocess
        errno = subprocess.call(['py.test',  'testing'])
        raise SystemExit(errno)

def read(*parts):
    # this method is courtesy of AutoQA and is GPL2+
    return codecs.open(os.path.join(here, *parts), 'r').read()

def find_version(*file_paths):
    # this method is courtesy of AutoQA and is GPL2+
    version_file = read(*file_paths)
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

# grab requirements from requirements.txt
with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(name='pyspringboard',
      version=find_version('pyspringboard', '__init__.py'),
      description='Platform for rapidly dispursing project configuration',
      author='PySprings',
      author_email='pysprings@pysprings.org',
      license='GPLv2+',
      url='https://bitbucket.org/pysprings/pyspringboard',
      packages=['pyspringboard'],
      package_dir={'pyspringboard':'pyspringboard'},
      entry_points=dict(console_scripts=['pyspring=pyspringboard:main']),
      include_package_data=True,
      cmdclass = {'test' : PyTest},
      install_requires = required
     )
