import sys
import os
import json
import argparse
import urllib
from ConfigParser import SafeConfigParser

import git
import venv

# single place to put the version
__version__ = "0.0.2"

supported_commands = ['clone']

class PyspringboardInit():
    def __init__(self, config=None, index=None):
        if config is not None:
            self.config = config
        else:
            self.config = self.get_config()

        if index is not None:
            self.index = index
        else:
            self.index = self.parse_json(self.config['index'])

    def parse_json(self, filename):
        contentfile = urllib.urlopen(filename)
        index =  json.load(contentfile)
        contentfile.close()
        return index

    def get_config(self):
        if os.path.exists('/etc/pyspringboard/pyspringboard.ini'):
            configfilename = '/etc/pyspringboard/pyspringboard.ini'
        elif os.path.exists('conf/pyspringboard.ini'):
            configfilename = os.path.abspath('conf/pyspringboard.ini')
        else:
            raise Exception('pyspringboard configuration not found in conf/ or /etc/pyspringboard. Exiting')

        configparse = SafeConfigParser()
        configparse.read(configfilename)

        config = {}
        config['index'] = configparse.get('main','index')

        return config

    def get_projectrepo(self, projectname):
        return self.index[projectname]['repolocation']

    def get_projectname(self, projectname):
        return self.index[projectname]['projectname']

    def init_project(self, projectname):
        projectrepo = self.get_projectrepo(projectname)
        name = self.get_projectname(projectname)

        self.clone_project(name, projectrepo)
        self.bootstrap_project(name)

    def clone_project(self, name , repo):
        repository = git.Repository(name, repo)
        repository.clone()
        repository.verify_tags()

    def bootstrap_project(self, name):
        print "Bootstrapping %s..." % name
        env = venv.VirtualEnv('env')
        env.create()
        env.install_requirements()

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('command', nargs=1, help='command to run. Acceptable input: %s' % ' '.join(supported_commands))
    parser.add_argument('args', nargs='*', help='input to command')

    args = parser.parse_args()

    if args.command[0] not in supported_commands:
        print "Must have a command in: %s" % ' '.join(supported_commands)
        sys.exit(1)

    pyspringboard_init = PyspringboardInit()

    if args.command[0] == 'clone':
        if len(args.args) > 0:
            command_args = args.args
            print 'cloning project for %s' % ' '.join(command_args)
            pyspringboard_init.init_project(command_args[0])

        else:
            print "need to have args for clone"
            sys.exit(1)

if __name__ == '__main__':
    main()
