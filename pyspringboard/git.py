import os

from sh import git, cd

verify_tag = git.bake('verify-tag')

class Repository(object):
    """
    Manages a project repo. Right now, that means handling the git
    clone and checking the tags for GPG signatures.
    """
    def __init__(self, name, repo):
        self.repo = repo
        self.name = name

    def clone(self):
        git.clone(self.repo, self.name)

    def verify_tags(self):
        cd(self.name)
        tags = git.tag(_iter=True)
        for tag in tags:
            print(verify_tag(tag.strip()).stderr)
