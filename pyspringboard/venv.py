from sh import Command

from sh import virtualenv as venv

class VirtualEnv(object):
    def __init__(self, name):
        self.name = name

    def create(self):
        venv(self.name)

    def install_requirements(self):
        pip = Command('env/bin/pip')
        pip.install(r='requirements.txt')
