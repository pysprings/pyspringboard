import pyspringboard

class TestProjectInit():
    def setup_method(self, method):
        self.refindex = {'testproj':{'projectname':'testproj', 'repolocation':'localhost:testproj'}}
        self.refconfig = {'index':'testindex.json'}
        self.testinit = pyspringboard.PyspringboardInit(config=self.refconfig, index=self.refindex)

    def test_projectinit_getrepo_fromindex(self):
        testrepo = self.testinit.get_projectrepo('testproj')

        assert testrepo == self.refindex['testproj']['repolocation']

    def test_projectinit_getname_fromindex(self):
        testname = self.testinit.get_projectname('testproj')

        assert testname == self.refindex['testproj']['projectname']

class TestProjectIndex():
    def setup_method(self, method):
        self.ref_jsonfile = 'testing/testindex.json'
        self.testinit = pyspringboard.PyspringboardInit()

    def test_parsejson(self):
        index = self.testinit.parse_json(self.ref_jsonfile)

        assert 'flask-intro' in index.keys()

    def test_getrepolocation(self):
        index = self.testinit.parse_json(self.ref_jsonfile)

        assert index['flask-intro']['projectname'] == 'flask-intro'


class TestConfig():
    def setup_method(self, method):
        self.testinit = pyspringboard.PyspringboardInit()

    def test_getconfig(self):
        config = self.testinit.get_config()

        assert 'index' in config.keys()
